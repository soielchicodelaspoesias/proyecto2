import random


def turno_player_one(tablero, texto, score_one):
    texto_v1 = ""
    prim_jugada_uno(jugador_uno, letras_uno)
    print("Turno jugador 1")
    print(jugador_uno)
    print("Si no puede escribir una palabra y quiere saltar el turno ingrese <0>: ")
    f = int(input("Si puede escribir una palabra ingrese <1>: "))
    if f == 0:
        jugabilidad_inversa(tablero, texto, score_one, score_two)
    d = int(input("<1> Vertical \n<2> Horizontal \n: "))
# vertical
    if d == 1:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, n + a):
            for j in range(b, b + 1):
                print(jugador_uno)
                a =  input("ingrese la letra: ")
                tablero[i][j] = a
                puntaje_uno(score_one, a)
                jugador_uno.remove(tablero[i][j])
                texto_v1 += tablero[i][j]
                imprimir(tablero, texto)
    print("El puntaje obtenido en este turno fue: ", score_one)
# horizontal
    if d == 2:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, a + 1):
            for j in range(b, n + b):
                print(jugador_uno)
                tablero[i][j] = input("ingrese la letra: ")
                jugador_uno.remove(tablero[i][j])
                texto_v1 += tablero[i][j]
                imprimir(tablero, texto)
    if d != 1 and d != 2:
        turno_player_one(tablero, texto, score_one)


# ___________________________________jugador dos_____________________________
def turno_player_two(tablero, texto, score_two):
    prim_jugada_dos(jugador_dos, letras_dos)
    print(jugador_dos)
    print("Turno jugador 2")
    texto_v2 = ""
    print("Si no puede escribir una palabra y quiere saltar el turno ingrese <0>: ")
    f = int(input("Si puede escribir una palabra ingrese <1>: "))
    if f == 0:
        jugabilidad(tablero, texto, score_one, score_two)
    d = int(input("<1> Vertical \n<2> Horizontal \n: "))
# vertical
    if d == 1:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, n + a):
            for j in range(b, b + 1):
                print(jugador_dos)
                tablero[i][j] = input("ingrese la letra: ")
                jugador_dos.remove(tablero[i][j])
                texto_v2 += tablero[i][j]
                imprimir(tablero, texto)
# horizontal
    if d == 2:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, a + 1):
            for j in range(b, n + b):
                print(jugador_dos)
                tablero[i][j] = input("ingrese la letra: ")
                jugador_dos.remove(tablero[i][j])
                texto_v2 += tablero[i][j]
                imprimir(tablero, texto)
    if d != 1 and d != 2:
        turno_player_two(tablero, texto, score_two)


def imprimir(tablero, texto):
    for i in range(16):
        for j in range(16):
            if i >= 10 and j >= 10:
                texto += ' ' + "|" + tablero[i][j] + " " + "|"
            else:
                texto += ' ' + "|" + tablero[i][j] + "|"
        texto += ' \n'
    print(texto)



def prim_jugada_uno(jugador_uno, letras_uno):
    for i in range(len(jugador_uno), 8):
        b = random.randint(0, 100)
        jugador_uno.append(letras_uno[b])
        letras_uno.remove(jugador_uno[i])
    return jugador_uno



def prim_jugada_dos(jugador_dos, letras_dos):
    for i in range(len(jugador_dos), 8):
        b = random.randint(0, 100)
        jugador_dos.append(letras_dos[b])
        letras_dos.remove(jugador_dos[i])
    return jugador_dos

def puntaje_uno(score_one, a):
    if a == 'A' or a == 'E' or a == 'I' or a == 'L' or a == 'N':
        score_one += 1
    if a == 'D' or a == 'G':
        score_one += 2
    if  a == 'B' or a == 'C' or a == 'M' or a == 'P':
        score_one += 3
    if a == 'F' or a == 'H' or a == 'V' or a == 'Y':
        score_one += 4
    if a == 'Ch' or a == 'Q':
        score_one += 5
    if a == 'J' or a == 'Ll' or a == 'Ñ' or a == 'Rr' or a == 'X':
        score_one += 8
    if a == 'Z':
        score_one += 10

jugador_uno = []
score_one = 0
jugador_dos = []
score_two = 0
texto = ""
tablero = ['   ', "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"],\
             ["1  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["2  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["3  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["4  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["5  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["6  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["7  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["8  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["9  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["10 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["11 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["12 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["13 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["14 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["15 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\



letras_uno = ['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'C', 'C', 'C', 'D', 'D',
         'D', 'D', 'D', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'F', 'G', 'G', 'H', 'H',
         'I', 'I', 'I', 'I', 'I', 'I', 'J', 'L', 'L', 'L', 'L', 'M', 'M', 'N', 'N', 'N', 'N', 'N', 'Ñ', 'O',
         'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'P', 'P', 'Q', 'R', 'R', 'R', 'R', 'R', 'S', 'S', 'S', 'S',
         'S', 'S', 'T', 'T', 'T', 'T', 'U', 'U', 'U', 'U', 'U', 'V', 'X', 'Y', 'Z', 'Ch', 'Rr', 'Ll', ' ', ' ']

letras_dos = ['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'C', 'C', 'C', 'D', 'D',
         'D', 'D', 'D', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'F', 'G', 'G', 'H', 'H',
         'I', 'I', 'I', 'I', 'I', 'I', 'J', 'L', 'L', 'L', 'L', 'M', 'M', 'N', 'N', 'N', 'N', 'N', 'Ñ', 'O',
         'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'P', 'P', 'Q', 'R', 'R', 'R', 'R', 'R', 'S', 'S', 'S', 'S',
         'S', 'S', 'T', 'T', 'T', 'T', 'U', 'U', 'U', 'U', 'U', 'V', 'X', 'Y', 'Z', 'Ch', 'Rr', 'Ll', ' ', ' ']

def jugabilidad(tablero, texto, score_one, score_two):
    while True:
        imprimir(tablero, texto)
        turno_player_one(tablero, texto, score_one)
        turno_player_two(tablero, texto, score_two)



def jugabilidad_inversa(tablero, texto, score_one, score_two):
    while True:
        imprimir(tablero, texto)
        turno_player_two(tablero, texto, score_two)
        turno_player_one(tablero, texto, score_one)


jugabilidad(tablero, texto, score_one, score_two)
