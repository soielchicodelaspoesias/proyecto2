import random

def turno_player_one(tablero):
    prim_jugada_uno(jugador_uno, letras_uno)
    print("Turno jugador 1")
    texto_v1 = ""
    d = int(input("<1> Vertical \n<2> Horizontal \n: "))
# vertical
    print(jugador_uno)
    if d == 1:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        print("Si se equivoca escribiendo la palabra, y desea volverla a escribir ingrese 'XX'")
        for i in range(a, n + a):
            for j in range(b, b + 1):
                print(jugador_uno)
                tablero[i][j] = input("ingrese la letra: ")
                jugador_uno.remove(tablero[i][j])
                texto_v1 += tablero[i][j]
                imprimir(tablero, texto)
# horizontal
    if d == 2:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, a + 1):
            for j in range(b, n + b):
                print(jugador_uno)
                tablero[i][j] = input("ingrese la letra: ")
                jugador_uno.remove(tablero[i][j])
                texto_v1 += tablero[i][j]
                imprimir(tablero, texto)
    if d != 1 and d != 2:
        vertical_horizontal_v1(player_one)


# ___________________________________jugador dos_____________________________
def turno_player_two(tablero):
    prim_jugada_dos(jugador_dos, letras_dos)
    print("Turno jugador 2")
    texto_v2 = ""
    d = int(input("<1> Vertical \n<2> Horizontal \n: "))
    print(jugador_dos)
# vertical
    if d == 1:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, n + a):
            for j in range(b, b + 1):
                print(jugador_dos)
                tablero[i][j] = input("ingrese la letra: ")
                jugador_dos.remove(tablero[i][j])
                texto_v2 += tablero[i][j]
                imprimir(tablero, texto)
# horizontal
    if d == 2:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, a + 1):
            for j in range(b, n + b):
                print(jugador_dos)
                tablero[i][j] = input("ingrese la letra: ")
                jugador_dos.remove(tablero[i][j])
                texto_v2 += tablero[i][j]
                imprimir(tablero, texto)
    if d != 1 and d != 2:
        vertical_horizontal_v2(player_two)


def imprimir(tablero, texto):
    for i in range(16):
        for j in range(16):
            if i >= 10 and j >= 10:
                texto += ' ' + "|" + tablero[i][j] + " " + "|"
            else:
                texto += ' ' + "|" + tablero[i][j] + "|"
        texto += ' \n'
    print(texto)



def prim_jugada_uno(jugador_uno, letras_uno):
    for i in range(8):
        b = random.randint(0, 100)
        jugador_uno.append(letras_uno[b])
        letras_uno.remove(jugador_uno[i])
    return jugador_uno



def prim_jugada_dos(jugador_dos, letras_dos):
    for i in range(8):
        b = random.randint(0, 100)
        jugador_dos.append(letras_dos[b])
        letras_dos.remove(jugador_dos[i])
    return jugador_dos



jugador_uno = []
jugador_dos = []
texto = ""
tablero = ['   ', "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"],\
             ["1  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["2  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["3  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["4  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["5  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["6  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["7  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["8  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["9  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["10 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["11 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["12 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["13 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["14 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["15 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\



letras_uno = ['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'C', 'C', 'C', 'D', 'D',
         'D', 'D', 'D', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'F', 'G', 'G', 'H', 'H',
         'I', 'I', 'I', 'I', 'I', 'I', 'J', 'L', 'L', 'L', 'L', 'M', 'M', 'N', 'N', 'N', 'N', 'N', 'Ñ', 'O',
         'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'P', 'P', 'Q', 'R', 'R', 'R', 'R', 'R', 'S', 'S', 'S', 'S',
         'S', 'S', 'T', 'T', 'T', 'T', 'U', 'U', 'U', 'U', 'U', 'V', 'X', 'Y', 'Z', 'Ch', 'Rr', 'Ll', ' ', ' ']

letras_dos = ['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'C', 'C', 'C', 'D', 'D',
         'D', 'D', 'D', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'F', 'G', 'G', 'H', 'H',
         'I', 'I', 'I', 'I', 'I', 'I', 'J', 'L', 'L', 'L', 'L', 'M', 'M', 'N', 'N', 'N', 'N', 'N', 'Ñ', 'O',
         'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'P', 'P', 'Q', 'R', 'R', 'R', 'R', 'R', 'S', 'S', 'S', 'S',
         'S', 'S', 'T', 'T', 'T', 'T', 'U', 'U', 'U', 'U', 'U', 'V', 'X', 'Y', 'Z', 'Ch', 'Rr', 'Ll', ' ', ' ']

imprimir(tablero, texto)
turno_player_one(tablero)
turno_player_two(tablero)
