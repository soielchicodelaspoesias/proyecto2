def imprimir_uno(player_one, texto):
    for i in range(16):
        for j in range(16):
            if i >= 10 and j >= 10:
                texto += ' ' + "|" + player_one[i][j] + " " + "|"
            else:
                texto += ' ' + "|" + player_one[i][j] + "|"
        texto += ' \n'
    print(texto)


texto = ""

player_one = ['   ', "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"],\
             ["1  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["2  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["3  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["4  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["5  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["6  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["7  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["8  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["9  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["10 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["11 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["12 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["13 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["14 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["15 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\


player_two = ['   ', "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"],\
             ["1  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["2  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["3  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["4  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["5  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["6  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["7  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["8  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["9  ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '  ', '  ', '  ', '  ', '  ', '  '],\
             ["10 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["11 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["12 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["13 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["14 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\
             ["15 ", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],\



def vertical_horizontal_v1(player_one):
    texto_v1 = ""
    d = int(input("<1> Vertical \n<2> Horizontal \n: "))
# vertical
    if d == 1:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        print("Si se equivoca escribiendo la palabra, y desea volverla a escribir ingrese 'XX'")
        for i in range(a, n + a):
            for j in range(b, b + 1):
                player_one[i][j] = input("ingrese la letra: ")
                texto_v1 += player_one[i][j]
                imprimir_uno(player_one, texto)
                if player_one[i][j] == "XX":
                    texto_v1 = ""
                    for i in range(a, n + a):
                        for j in range(b, b + 1):
                            player_one[i][j] = input("ingrese la letra: ")
                            texto_v1 += player_one[i][j]
                            imprimir_uno(player_one, texto)
# horizontal
    if d == 2:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, a + 1):
            for j in range(b, n + b):
                player_one[i][j] = input("ingrese la letra: ")
                texto_v1 += player_one[i][j]
                imprimir_uno(player_one, texto)
                if player_one[i][j] == "XX":
                    texto_v1 = ""
                    for i in range(a, a + 1):
                        for j in range(b, n + b):
                            player_one[i][j] = input("ingrese la letra: ")
                            imprimir_uno(player_one, texto)
                            texto_v1 += player_one[i][j]
    if d != 1 and d != 2:
        vertical_horizontal_v1(player_one)


def vertical_horizontal_v2(player_two):
    texto_v2 = ""
    d = int(input("<1> Vertical \n<2> Horizontal \n: "))
# vertical
    if d == 1:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, n + a):
            for j in range(b, b + 1):
                player_two[i][j] = input("ingrese la letra: ")
                texto_v2 += player_two[i][j]
                imprimir_dos(player_two, texto)
                if player_two[i][j] == "XX":
                    texto_v2 = ""
                    for i in range(a, n + a):
                        for j in range(b, b + 1):
                            player_two[i][j] = input("ingrese la letra: ")
                            texto_v2 += player_two[i][j]
                            imprimir_dos(player_two, texto)
# horizontal
    if d == 2:
        n = int(input("Ingrese el tamaño de la palabra: "))
        if n > 15:
            print("el tamaño de la palabra es muy grande, no cae en el tablero")
            n = int(input("Ingrese el tamaño de la palabra: "))
        a = int(input("ingrese la fila: "))
        b = int(input("ingrese la columna: "))
        for i in range(a, a + 1):
            for j in range(b, n + b):
                player_one[i][j] = input("ingrese la letra: ")
                texto_v2 += player_two[i][j]
                imprimir_dos(player_two, texto)
                if player_two[i][j] == "XX":
                    texto_v2 = ""
                    for i in range(a, a + 1):
                        for j in range(b, n + b):
                            player_one[i][j] = input("ingrese la letra: ")
                            texto_v2 += player_two[i][j]
                            imprimir_dos(player_two, texto)
    if d != 1 and d != 2:
        vertical_horizontal_v2(player_two)



def imprimir_uno(player_one, texto):
    print("Tablero jugador 1")
    for i in range(16):
        for j in range(16):
            if i >= 10 and j >= 10:
                texto += ' ' + "|" + player_one[i][j] + " " + "|"
            else:
                texto += ' ' + "|" + player_one[i][j] + "|"
        texto += ' \n'
    print(texto)

def imprimir_dos(player_two, texto):
    print("Tablero jugador 2")
    for i in range(16):
        for j in range(16):
            if i >= 10 and j >= 10:
                texto += ' ' + "|" + player_two[i][j] + " " + "|"
            else:
                texto += ' ' + "|" + player_two[i][j] + "|"
        texto += ' \n'
    print(texto)


imprimir_dos(player_two, texto)
imprimir_uno(player_one, texto)
vertical_horizontal_v1(player_one)
vertical_horizontal_v2(player_two)
